import json, logging, time, os
from kafka import KafkaConsumer
#import app
from crud import last_login

logger = logging.getLogger('ac')

topic = "user_topic"

d = os.path.dirname(__file__)

consumer = KafkaConsumer(
    topic,
    bootstrap_servers=['kafka:9092'],
    value_deserializer=lambda m: json.loads(m.decode('ascii'))
)
while True:
    for msg in consumer:
        u_id = msg.value.pop('id')
        data = msg.value
        try:
            last_login(u_id, data)
        except:
            logger.warning("Some wrongs")
        logger.warning(f"Msg {u_id} - at {data} ")
    time.sleep(10)
