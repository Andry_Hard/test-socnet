from db import sync_db_connect


def last_login(id, data):
    db = sync_db_connect()
    return db['user_data'].update_one(
        {'_id': id},
        {"$set": data},
        upsert=True
    )
