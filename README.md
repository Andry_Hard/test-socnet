# Test Social network blog
==========================

The Simple blog. 

Blog with different reactions features and possibility to analyze count of these.

1 Installation
--------------

> git clone https://gitlab.com/Andry_Hard/test-socnet.git

Move to directory and add "data":

> cd test-socnet
> mkdir data

2 Run the project
-----------------

Use "docker" to run the project

>docker-compose up --build


