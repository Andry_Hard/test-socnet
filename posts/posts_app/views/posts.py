import logging
from datetime import datetime
from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import JSONResponse
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jwt import PyJWTError
from pydantic import Json
from typing import List, Union
from slugify import slugify
from posts_app.db import db_connect
from posts_app.models import UserLogin, PostBase, Post, Reaction, ReactionStatus
from posts_app.functions.post_crud import (
    create_post,
    all_posts,
    get_post,
    reaction_post
)
from posts_app.functions.auth_utils import get_current_user
from posts_app.broker import producer


logger = logging.getLogger('post')

router = APIRouter()


@router.get('/')
async def index(user = Depends(get_current_user)):
    print(user)
    all = await all_posts()
    return all


@router.post('/', status_code=201)
async def create(
    data: PostBase,
    user = Depends(get_current_user)
) -> Post:
    new_post = data.dict()
    new_post['create_by'] = user.get('id')
    try:
        new = await create_post(new_post)
        result = await get_post(new.inserted_id)
        return result
    except:
        return JSONResponse(content={"result": "Bad"}, status_code=422)


@router.get('/{post_id}/')
async def post(
    post_id: str,
    user = Depends(get_current_user)
) -> Post:
    post = await get_post(post_id)
    if not post:
        raise HTTPException(status_code=404)
    return post


@router.patch('/{post_id}/like/')
async def post_like(
    post_id: str,
    user = Depends(get_current_user)
):
    post = await get_post(post_id)
    if not post:
        raise HTTPException(status_code=404)
    data = {
        "user_id": user.get('id'),
        "action": ReactionStatus.like
    }
    await reaction_post(post_id, data)
    producer.send('user_topic', {
        'id': user.get('id'),
        'last_interaction': f"{datetime.utcnow()}"
    })
    producer.send('post_topic', {
        "id": post_id,
        "create_by": post.get("create_by"),
        "user_id": user.get('id'),
        "action": ReactionStatus.like,
        "date": f"{datetime.today()}"
    })
    return {"detail": "post is liked"}


@router.patch('/{post_id}/dislike/')
async def post_dislike(
    post_id: str,
    user = Depends(get_current_user)
):
    post = await get_post(post_id)
    if not post:
        raise HTTPException(status_code=404)
    data = {
        "user_id": user.get('id'),
        "action": ReactionStatus.dislike
    }
    await reaction_post(post_id, data)
    producer.send('user_topic', {
        'id': user.get('id'),
        'last_interaction': f"{datetime.utcnow()}"
    })
    producer.send('post_topic', {
        "id": post_id,
        "create_by": post.get("create_by"),
        "user_id": user.get('id'),
        "action": ReactionStatus.dislike,
        "date": f"{datetime.today()}"
    })
    return {"detail": "post is disliked"}
