# import db-connector
#
import os
import pymongo
import motor.motor_asyncio
import logging


POSTS_HOST = os.environ.get("POSTS_DB_HOST")

logger = logging.getLogger('posts')

def sync_db_connect():
    """This is sync database connector"""
    try:
        client = pymongo.MongoClient(POSTS_HOST, 27017)
        try:
            db = client["star_db"]
            return db
        except Exception as e:
            logger.error("Database error", exc_info=True)
    except Exception as e:
        logger.error("Database connection error", exc_info=True)

async def db_connect():
    """This is async database connector"""
    try:
        client = motor.motor_asyncio.AsyncIOMotorClient(POSTS_HOST, 27017)
        try:
            db = client["star_db"]
            return db
        except Exception as e:
            logger.error("Database error", exc_info=True)
    except Exception as e:
        logger.error("Database connection error", exc_info=True)

async def db_disconnect():
    pass
