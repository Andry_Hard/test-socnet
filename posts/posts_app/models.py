import json
from datetime import datetime, date
from typing import List, Dict
from pydantic import BaseModel, Json, validator
from enum import Enum


class ReactionStatus(str, Enum):
    '''
    Статуси реакцій 
    '''
    like = 'Like'
    dislike = 'Dislike'

################################################################################
# Category models
################################################################################
class Reaction(BaseModel):
    user_id: int
    status: ReactionStatus


class PostBase(BaseModel):
    title: str
    body: str

class Post(PostBase):
    user_id: int
    reactions: List[Reaction] = []

################################################################################
# User
################################################################################
class User(BaseModel):
    id: str
    email: str
    created_date: str
    is_active: bool = True

class UserLogin(BaseModel):
    email: str
    password: str
