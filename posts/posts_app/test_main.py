import pytest, json
from fastapi.testclient import TestClient
from slugify import slugify
from products_app.main import app
from products_app.functions import category_crud


@pytest.fixture(scope="module")
def test_app():
    client = TestClient(app)
    yield client

#def test_create_category(test_app, monkeypatch):
#    test_request_data = {
#        "title": "stringdfdf3",
#        "description": "string",
#        "parent": "string"
#    }
#    test_response_data = {
#        "_id": "stringdfdf3",
#        "title": "stringdfdf",
#        "description": "string",
#        "status": "IS_VALIDATE",
#        "parent": "string"
#    }

#    async def mock_post(payload):
#        return

#    monkeypatch.setattr(category_crud, "create_category", mock_post)

#    response = test_app.post("/categories/", data=json.dumps(test_request_data),)

#    assert response.status_code == 201


def test_get_category(test_app):
    test_query = "stringdfdf3"
    test_response_data = {
        "_id": "stringdfdf3",
        "title": "stringdfdf3",
        "description": "string",
        "status": "IS_VALIDATE",
        "parent": "string"
    }

    response = test_app.get(f'/categories/{test_query}/')
    assert response.status_code == 200
    assert response.json() == test_response_data

def test_wrong_category(test_app):
    test_query = "wrong"
    response = test_app.get(f'/categories/{test_query}/')
    assert response.status_code == 404

def test_all_products(test_app):
    response = test_app.get(f'/products/')
    assert response.status_code == 200

def test_get_product(test_app):
    test_query = "stringdfdf3"
    test_response_data = {
        "_id": "stringdfdf3",
        "title": "stringdfdf3",
        "description": "string",
        "status": "IS_VALIDATE",
        "parent": "string",
        "parent_all": ["string"],
        "prices_all": []
    }

    response = test_app.get(f'/products/{test_query}/')
    assert response.status_code == 200
    assert response.json() == test_response_data
