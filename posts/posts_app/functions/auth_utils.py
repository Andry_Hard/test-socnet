from datetime import datetime, timedelta

import jwt, os, logging
from fastapi import Depends, FastAPI, HTTPException, status, Header, Request
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jwt import PyJWTError
from posts_app.models import User

logger = logging.getLogger('auth')

SECRET_KEY = os.environ.get("AUTH_SECRET_KEY")

ALGORITHM = os.environ.get("ALGORITHM")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/api/auth/signin/")

credentials_exception = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Could not validate credentials",
    headers={"WWW-Authenticate": "Bearer"},
)


async def get_token(request: Request):
    try:
        auth_header = request.headers.get('Authorization')
        if auth_header:
            return auth_header.split(' ')[1]
    except PyJWTError:
        raise credentials_exception


async def get_current_user(token = Depends(get_token)) -> User:
    try:
        logger.warning(f"Token - {token}")
        user = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        return user
    except PyJWTError:
        raise credentials_exception
