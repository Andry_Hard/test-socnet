import logging
from fastapi import HTTPException
from slugify import slugify
#from posts_app.models import CategoryBase
from posts_app.db import db_connect
from posts_app.functions.base_utils import uniq

logger = logging.getLogger('post')


async def all_posts():
    db = await db_connect()
    all = []
    async for post in db['posts'].find({}):
        all.append(post)
    return all


async def create_post(data):
    db = await db_connect()
    data['_id'] = f"{slugify(data['title'])}_{uniq()}"
    return await db['posts'].insert_one(data)


async def get_post(post_id: str):
    db = await db_connect()
    return await db['posts'].find_one({'_id':post_id})


async def reaction_post(post_id, data):
    db = await db_connect()
    return await db['posts'].update_one(
        {'_id': post_id},
        {"$set": {"reaction": data}},
        upsert=True
    )
