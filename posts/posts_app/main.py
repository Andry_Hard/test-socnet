from fastapi import FastAPI, Depends
from posts_app.views import posts
from posts_app.functions.auth_utils import get_current_user, get_token


app = FastAPI(openapi_prefix="/api/posts")


app.include_router(
    posts.router,
    dependencies=[Depends(get_token)]
)


@app.get('/')
async def index():
    return {"hello": "Posts app"}

@app.on_event("startup")
async def startup():
    print("Start")


@app.on_event("shutdown")
async def shutdown():
    print("finish")
