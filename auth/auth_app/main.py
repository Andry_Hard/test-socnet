import asyncio, json, uuid
from fastapi import FastAPI
from aiokafka import AIOKafkaProducer
from auth_app.db import engine, metadata, database
from auth_app.views import register, signin
#from auth_app.broker import producer #, consumer

loop = asyncio.get_event_loop()
aioproducer = AIOKafkaProducer(
    loop=loop, client_id="auth", bootstrap_servers="kafka:9092",
    value_serializer=lambda v: json.dumps(v).encode('ascii')
)

metadata.create_all(engine)
app = FastAPI(
    openapi_prefix="/api/auth",
)

app.include_router(register.router)
app.include_router(signin.router)


@app.get('/')
async def index():
#    producer.send("my_topic", {"message": "index page"})
    await aioproducer.send("user_topic", {
        "id": f"{uuid.uuid4()}",
        "message": "async index page"})
    return {"hello": "New social network"}


@app.on_event("startup")
async def startup():
    await database.connect()
    await aioproducer.start()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()
