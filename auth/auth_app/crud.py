from auth_app.models import UserRegister, UserDetail
from auth_app.db import users, database
from auth_app.functions.utils import get_password_hash


async def post(data: UserRegister):
    query = users.insert().values(
        email=data.username,
        password=get_password_hash(data.password))
    return await database.execute(query=query)

async def get_user(username):
    query = users.select().where(
        users.c.email==username,
    )
    return await database.fetch_one(query=query)

async def get_all():
    query = users.select()
    return await database.fetch_all(query=query)
