import json
from datetime import datetime, date
from typing import List, Dict
from pydantic import BaseModel, Json, validator#, UrlStr
from enum import Enum


class UserBase(BaseModel):
    username: str


class UserToken(BaseModel):
    token: str


class UserRegister(UserBase):
    password: str
    password_again: str


class UserLogin(UserBase):
    password: str
    grand_type: str = 'password'


class UserDetail(UserBase):
    id: str
    created_date: str
    is_active: bool = True
