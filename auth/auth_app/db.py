import os
from databases import Database
from sqlalchemy import create_engine, MetaData
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Table, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
# імпортуємо синхронний і асинхронний клієнт для PostgreSQL


THIS_ENV = os.environ.get('THIS_ENV')

USER = os.environ.get('POSTGRES_USER')
PASS = os.environ.get('POSTGRES_PASSWORD')
DB_NAME = os.environ.get("POSTGRES_DB")

DATABASE_URL = f'postgresql://{USER}:{PASS}@auth-db/{DB_NAME}'


engine = create_engine(DATABASE_URL)
metadata = MetaData()

users = Table(
    "users",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("email", String(50), unique=True),
    Column("password", String(250)),
    Column("created_date", DateTime, default=func.now(), nullable=False),
    Column("is_active", Boolean, default=False),
)
database = Database(DATABASE_URL)
