import json
from kafka import KafkaProducer, KafkaConsumer, KafkaClient


producer = KafkaProducer(
    bootstrap_servers='kafka:9092',
    retry_backoff_ms=50000,
    value_serializer=lambda v: json.dumps(v).encode('ascii')
    )

#consumer = KafkaConsumer(
#        'my_topic', 'my_other_topic',
#        bootstrap_servers='kafka:9092',
#        group_id="my-group")
