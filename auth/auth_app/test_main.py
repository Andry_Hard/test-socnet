import pytest
import json
from fastapi.testclient import TestClient

from auth_app.main import app
from auth_app.functions.user_functions import verify_password, authenticate_user
from auth_app import crud


@pytest.fixture(scope="module")
def test_app():
    client = TestClient(app)
    yield client

def test_initial(test_app):
    response = test_app.get('/')
    assert response.status_code == 200
    assert response.json() == {"hello": "Faina specia"}

def test_create_user(test_app, monkeypatch):
    test_request_data = {"email": "test@t.test", "password": "pass3456", "password_again":"pass3456"}
    test_response_data =  {"id":1, "email": "test@t.test"}
    async def mock_post(payload):
        return 1

    monkeypatch.setattr(crud, "post", mock_post)

    response = test_app.post("/signup/", data=json.dumps(test_request_data),)

    assert response.status_code == 201
    assert response.json() == test_response_data

def test_incorrect_password(test_app):
    test_request_data = {"email": "test@t.test", "password": "pass3456", "password_again":"pas3456"}
    response = test_app.post("/signup/", data=json.dumps(test_request_data),)
    assert response.status_code == 422

def test_password_hash():
    test_password1 = "TestPa$$word20202"
    result1 = '$2b$12$KMi/PvHS4E8cv5wzjADkFOcCuwtwQj9wpQfDJP9Kl6ImquCtfz9oS'
    assert verify_password(test_password1, result1)

def test_verify_user():
    fake_users_db = {
    "johndoe@example.com": {
        "username": "johndoe",
        "full_name": "John Doe",
        "email": "johndoe@example.com",
        "password": "$2b$12$m4IjQgs3IMX73h/JNfVf1e6IW..GoyTNZpFWd29ymMOULz/gGxBtG",
        "disabled": False,
    },
    "alicechains@example.com": {
        "username": "alice",
        "full_name": "Alice Chains",
        "email": "alicechains@example.com",
        "password": "$2b$12$7kR.gxbSoaornXQdHGeXQ.gkOSsoL4kJGR4RxebCP48/cZzxuyuI6",
        "disabled": True,
    },
    }

    pass1 = "TestPa$$word"
    pass2 = "Pa$$word2"
    assert authenticate_user(fake_users_db, "johndoe@example.com", pass1)
    assert authenticate_user(fake_users_db, "alicechains@example.com", pass2)

def test_signin(test_app):
    fake_users_db = {
    "johndoe@example.com": {
        "username": "johndoe",
        "full_name": "John Doe",
        "email": "johndoe@example.com",
        "password": "$2b$12$m4IjQgs3IMX73h/JNfVf1e6IW..GoyTNZpFWd29ymMOULz/gGxBtG",
        "disabled": False,
    },
    "alicechains@example.com": {
        "username": "alice",
        "full_name": "Alice Chains",
        "email": "alicechains@example.com",
        "password": "$2b$12$7kR.gxbSoaornXQdHGeXQ.gkOSsoL4kJGR4RxebCP48/cZzxuyuI6",
        "disabled": True,
    },
    }
    test_request_data = {'email': 'johndoe@example.com', 'password': "TestPa$$word"}

    response = test_app.post("/signin/", data=json.dumps(test_request_data),)
    assert response.status_code == 200
    assert response.json() == {
        "username": "johndoe",
        "full_name": "John Doe",
        "email": "johndoe@example.com",
    }
