import logging, json
from datetime import datetime
from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jwt import PyJWTError
from auth_app.models import UserLogin
from auth_app.crud import get_user, get_all
#from auth_app.broker import producer#, consumer
from auth_app.functions.user_functions import (
    create_access_token,
    verify_password
)

logger = logging.getLogger('auth')

router = APIRouter()


@router.post('/signin/')
async def signin(data: UserLogin):
    user = await get_user(data.username)
    if user:
        is_authenticated = verify_password(data.password, user.get('password'))
        if is_authenticated:
            curent_user = {
                "id": user.get('id'),
                "email": user.get('email'),
                "is_active": user.get("is_active")
            }
            token = create_access_token(curent_user)
            await aioproducer.send('user_topic', {
                "id": user.get('id'),
                "last_login": f"{datetime.utcnow()}"
            })
            return {"token":token, "token_type": "bearer"}
        return JSONResponse(
            {"detail": "Password is incorrect"},
            status_code=404
        )
    return JSONResponse(
        {"detail": "User does not exist"},
        status_code=404
    )

@router.get('/users/')
async def all_users():
    all = await get_all()
    return all
