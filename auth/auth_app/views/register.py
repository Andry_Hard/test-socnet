from auth_app import crud
from fastapi import APIRouter, Response
from fastapi.responses import JSONResponse
from auth_app.models import UserRegister

router = APIRouter()


@router.post('/signup/')
async def signup(data:UserRegister):
    if data.password == data.password_again:
        user_id = await crud.post(data)
        # Додати функцію запиту в базу з отриманим ID
        user = {
            "id": user_id,
            "email": data.email,
        }
        return JSONResponse(content=user, status_code=201)
    else:
        return JSONResponse(content={"detail":"password is incorrect"}, status_code=422)
