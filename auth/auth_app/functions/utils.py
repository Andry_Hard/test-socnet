import os
from passlib.context import CryptContext


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

SECRET_KEY = os.environ.get('AUTH_SECRET_KEY')

ALGORITHM = os.environ.get('ALGORITHM')

def get_password_hash(password):
    return pwd_context.hash(password)
