import logging
from fastapi import HTTPException
from slugify import slugify
#from posts_app.models import CategoryBase
from analytics_app.db import db_connect

logger = logging.getLogger('post')


async def all_users():
    db = await db_connect()
    all = []
    async for post in db['user_data'].find({}):
        all.append(post)
    return all
