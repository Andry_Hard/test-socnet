import string
import random
import hashlib
import uuid
from datetime import datetime
from base64 import b64encode, b64decode


TODAY = datetime.strftime(datetime.today(), "%d-%m-%Y")

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


def make_slug() -> str:
    chars = string.ascii_lowercase+string.digits
    rand = ''.join(random.choice(chars) for x in range(10))
    hd = hashlib.sha256(TODAY.encode()).hexdigest()
    slug = f'{rand}_{hd}'
    return slug

def uniq():
    return uuid.uuid4()
