from fastapi import APIRouter
from analytics_app.functions.user_query import all_users

router = APIRouter()


@router.get('/users')
async def users():
    all = await all_users()
    return all
