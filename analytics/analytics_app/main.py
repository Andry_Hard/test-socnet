from fastapi import FastAPI, Depends
from analytics_app.views import analytics


app = FastAPI(openapi_prefix="/api/analytics")


app.include_router(analytics.router)


@app.get('/')
async def index():
    return {"hello": "Posts app"}

@app.on_event("startup")
async def startup():
    print("Start")


@app.on_event("shutdown")
async def shutdown():
    print("finish")
